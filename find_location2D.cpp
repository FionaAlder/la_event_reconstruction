#include <string>
#include <sstream>
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdlib>
using namespace std;

template<typename T>
std::vector<double> linspace(T start_in, T end_in, int num_in){

  std::vector<double> linspaced;

  double start = static_cast<double>(start_in);
  double end = static_cast<double>(end_in);
  double num = static_cast<double>(num_in);

  if (num == 0) { return linspaced; }
  if (num == 1){
      linspaced.push_back(start);
      return linspaced;
    }

  double delta = (end - start) / (num - 1);

  for(int i=0; i < num-1; ++i){
      linspaced.push_back(start + delta * i);
    }
  linspaced.push_back(end); // I want to ensure that start and end
                            // are exactly the same as the input
  return linspaced;
}

void print_vector(std::vector<double> vec){
  std::cout << "size: " << vec.size() << std::endl;
  for (double d : vec)
    std::cout << d << " ";
  std::cout << std::endl;
}

vector<double> x = linspace(-200.0, 200.0, 101);
vector<double> y = linspace(-200.0, 200.0, 101);
vector<double> c = linspace(0.0, 10000.0, 10001); //maybe start from 300??

//double xp[] {-14.3,71.8,14.3,-71.8,61.1,61.1,-61.1,-61.1};
//double yp[] {71.8,14.3,-71.8,-14.3,51.3,-51.3,51.3,-51.3};
//double zp[] {180.1,180.1,180.1,180.1,-14.8,-14.8,-14.8,-14.8};
//double zp[] {97.5,97.5,97.5,97.5,-97.5,-97.5,-97.5,-97.5};

double xp[4]={-14.3,71.8,14.3,-71.8}; //,61.1,61.1,-61.1,-61.1};
double yp[4]={71.8,14.3,-71.8,-14.3}; //,51.3,-51.3,51.3,-51.3};

typedef vector< vector<double> > Matrix;
typedef vector<double> Row;

Matrix get_overlap(double V1up, double V2up, double V3up, double V4up, double V1low, double V2low, double V3low, double V4low, double c){

  const size_t N = 2; // the dimension of the matrix
  Matrix points1;
  Matrix points2;
  Matrix points3;
  Matrix points4;

  Matrix overlap1Points;
  Matrix overlap2Points;
  Matrix overlap3Points;

  double RHS1low = c/V1up;
  double RHS2low = c/V2up;
  double RHS3low = c/V3up;
  double RHS4low = c/V4up;
  double RHS1up = c/V1low;
  double RHS2up = c/V2low;
  double RHS3up = c/V3low;
  double RHS4up = c/V4low;

  for (int i=0; i<x.size(); i++){
    for (int j=0; j<y.size(); j++){
        double LHS1 = pow((x[i]-xp[0]),2) + pow((y[j]-yp[0]),2);
        double LHS2 = pow((x[i]-xp[1]),2) + pow((y[j]-yp[1]),2);
        double LHS3 = pow((x[i]-xp[2]),2) + pow((y[j]-yp[2]),2);
        double LHS4 = pow((x[i]-xp[3]),2) + pow((y[j]-yp[3]),2);

        if (RHS1low <= LHS1 && LHS1 <= RHS1up){
          Row row1(N);
          row1[0] = x[i];
          row1[1] = y[j];
          points1.push_back(row1);
        }
        if (RHS2low <= LHS2 && LHS2 <= RHS2up){
          Row row2(N);
          row2[0] = x[i];
          row2[1] = y[j];
          points2.push_back(row2);
        }
        if (RHS3low <= LHS3 && LHS3 <= RHS3up){
          Row row3(N);
          row3[0] = x[i];
          row3[1] = y[j];
          points3.push_back(row3);
        }
        if (RHS4low <= LHS4 && LHS4 <= RHS4up){
          Row row4(N);
          row4[0] = x[i];
          row4[1] = y[j];
          points4.push_back(row4);
        }
      }
    }

  // std::cout<<"Points1 = "<<points1.size()<<'\n';
  // std::cout<<"Points2 = "<<points2.size()<<'\n';
  // std::cout<<"Points3 = "<<points3.size()<<'\n';
  // std::cout<<"Points4 = "<<points4.size()<<'\n';

  for (int i=0; i<points1.size(); i++){
    for (int o=0; o<points2.size(); o++){
      if (points1[i][0]==points2[o][0] && points1[i][1]==points2[o][1]){
        Row row5(N);
        row5[0] = points1[i][0];
        row5[1] = points1[i][1];
        overlap1Points.push_back(row5);
      }
    }
  }

  if (overlap1Points.size() > 0){
    //std::cout<<"first hurdle"<<'\n';
    for (int i=0; i<overlap1Points.size(); i++){
      for (int o=0; o<points3.size(); o++){
        if (overlap1Points[i][0]==points3[o][0] && overlap1Points[i][1]==points3[o][1]){
          Row row6(N);
          row6[0] = overlap1Points[i][0];
          row6[1] = overlap1Points[i][1];
          overlap2Points.push_back(row6);
        }
      }
    }
  }

  if (overlap2Points.size() > 0){
    //std::cout<<"second hurdle"<<'\n';
    for (int i=0; i<overlap2Points.size(); i++){
      for (int o=0; o<points4.size(); o++){
        if (overlap2Points[i][0]==points4[o][0] && overlap2Points[i][1]==points4[o][1]){
          Row row7(N);
          row7[0] = overlap2Points[i][0];
          row7[1] = overlap2Points[i][1];
          overlap3Points.push_back(row7);
        }
      }
    }
  }
  // std::cout<<"overlap1Points = "<<overlap1Points.size()<<'\n';
  // std::cout<<"overlap2Points = "<<overlap2Points.size()<<'\n';
  // std::cout<<"overlap3Points = "<<overlap3Points.size()<<'\n';

  if (overlap3Points.size() > 0){
    std::cout<<"overlap3Points = "<<overlap3Points.size()<<'\n';
    std::cout<<"we actually have some overlap!!!!!!!"<<'\n';
  }

  return(overlap3Points);
}

//int main(){
void find_location2D(){

    ifstream in("noise_data_for_pos.csv");
    //ifstream in("background_data.csv");
    //ifstream in("la_extraction_region_test_cpp.csv");

    string line, field;

    vector< vector<string> > array;
    vector<string> v;

    while ( getline(in,line) ){    // get next line in file
        v.clear();
        stringstream ss(line);
        while (getline(ss,field,',')){  // break line into comma delimitted fields
            v.push_back(field);  // add each field to the 1D array
        }
        array.push_back(v);  // add the 1D array to the 2D array
    }

    int size = array.size() - 1;
    double LA5728[size];
    double LA5727[size];
    double LA5726[size];
    double LA5725[size];

    double V1up[size];
    double V2up[size];
    double V3up[size];
    double V4up[size];
    double V1low[size];
    double V2low[size];
    double V3low[size];
    double V4low[size];

    for (int i=1; i<array.size(); i++){
      LA5728[i-1] = stod(array[i][6]);
      LA5727[i-1] = stod(array[i][13]);
      LA5726[i-1] = stod(array[i][20]);
      LA5725[i-1] = stod(array[i][27]);
      // LA5728[i-1] = stod(array[i][9]);
      // LA5727[i-1] = stod(array[i][19]);
      // LA5726[i-1] = stod(array[i][29]);
      // LA5725[i-1] = stod(array[i][39]);
    }


    for (int i=0; i<size; i++){
      V1up[i] = 1.25 * LA5728[i];
      V2up[i] = 1.25 * LA5727[i];
      V3up[i] = 1.25 * LA5726[i];
      V4up[i] = 1.25 * LA5725[i];
      V1low[i] = 0.75 * LA5728[i];
      V2low[i] = 0.75 * LA5727[i];
      V3low[i] = 0.75 * LA5726[i];
      V4low[i] = 0.75 * LA5725[i];
    }

    Matrix plotPoints1;
    Matrix plotPoints2;
    Matrix plotPoints3;
    Matrix plotPoints4;
    Matrix plotoverlap3Points;

    ofstream outdata;
    double c_final[100];


    for (int i=0; i<100; i++){
      std::cout<<"i= "<<i<<'\n';
      for (int o=0; o<c.size(); o++){
        //plotPoints1,plotPoints2,plotPoints3,plotPoints4,plotoverlap3Points = get_overlap(V1[i],V2[i],V3[i],V4[i],c[o]);
        plotoverlap3Points = get_overlap(V1up[i],V2up[i],V3up[i],V4up[i],V1low[i],V2low[i],V3low[i],V4low[i],c[o]);
        if (plotoverlap3Points.size() > 0){
          c_final[i]=c[o];
          std::cout<<"c final = "<<c[o]<<'\n';
          std::cout<<"............"<<'\n';
          break;
        }
      }

      // outdata.open("LocationOutput_background/c_values_"+to_string(i)+".csv"); // opens the file
      // if( !outdata ) { // file couldn't be opened
      //   cerr << "Error: file could not be opened" << endl;
      //   exit(1);
      // }
      // outdata << "c"<< endl;
      // for (int p=0; p<42; p++){
      //   //outdata << plotoverlap3Points[p] << "," << endl;
      //   outdata << c_final[p] << endl;
      // }
      // outdata.close();

      //outdata.open("LocationOutput_shell_background/OverlapPoints_"+to_string(i)+".csv"); // opens the file
      outdata.open("LocationOutput_shell_noise_14_07/OverlapPoints_"+to_string(i)+".csv"); // opens the file
      if( !outdata ) { // file couldn't be opened
        cerr << "Error: file could not be opened" << endl;
        exit(1);
      }
      outdata << "overlapx,overlapy"<< endl;
      for (int p=0; p<plotoverlap3Points.size(); p++){
        outdata << plotoverlap3Points[p][0] << "," << plotoverlap3Points[p][1] << endl;
        //outdata << c_final[p] << endl;
      }
      outdata.close();
    }
      return 0;
  }
